package com.coinbase.api.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.coinbase.api.dao.PurchasedProductDAO;
import com.coinbase.api.proxy.ProductCoinBase;
import com.coinbase.api.proxy.ProxyCoinBase;
import com.coinbase.api.repository.ProductRepository;

import io.swagger.model.Product;
import io.swagger.model.Stats;

@Service
public class CoinBaseService {

	private static final Logger logger = LoggerFactory.getLogger(CoinBaseService.class);

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProxyCoinBase proxyCoinBase;

	@Autowired
	private ProductCoinBase productCoinBase;

	@Value("${com.coinbase.api.products.stats.url}")
	private String productStatsUrl;

	@Value("${com.coinbase.api.products.list.url}")
	private String productListUrl;

	public List<Stats> calculateMostLoss(int maxNumberCrypto) {

		List<Stats> statsList = new ArrayList<>();

		try {
			List<Product> productList = productCoinBase.getProducts().getBody();

			if (productList != null) {
				List<String> productIdList = productList.stream().map(Product::getId).collect(Collectors.toList());
				Map<String, BigDecimal> percentageMap = new HashMap<>();
				Map<String, Stats> productMap = new HashMap<>();

				for (String productId : productIdList) {
					Thread.sleep(150);
					ResponseEntity<Stats> statsResponse = proxyCoinBase.getStatsById(productId);
					Stats stats = statsResponse.getBody();

					if (stats != null) {
						BigDecimal last = new BigDecimal(stats.getLast());
						BigDecimal open = new BigDecimal(stats.getOpen());

						if ((last.subtract(open)).compareTo(BigDecimal.ZERO) < 0) {
							percentageMap.put(productId, last.subtract(open).divide(open, 4, RoundingMode.HALF_DOWN));
							productMap.put(productId, stats);
						}
					}
				}

				Map<String, BigDecimal> sortedPercentage = new LinkedHashMap<>();

				percentageMap.entrySet().stream().sorted(Map.Entry.comparingByValue())
						.forEachOrdered(x -> sortedPercentage.put(x.getKey(), x.getValue()));

				printMap(sortedPercentage);

				int i = 1;

				for (Map.Entry<String, BigDecimal> entry : sortedPercentage.entrySet()) {

					logger.info("{}° id-product: {}, percentage-variation: {}, stats: {}", i, entry.getKey(),
							entry.getValue(), productMap.get(entry.getKey()));

					statsList.add(productMap.get(entry.getKey()));

					PurchasedProductDAO productBought = new PurchasedProductDAO();
					BigDecimal last = new BigDecimal(productMap.get(entry.getKey()).getLast());

					productBought.setIdCrypto(entry.getKey());
					productBought.setPurchasePrice(last);
					productBought.setSellProduct(0);
					productBought.setStartPercentage(entry.getValue());
					productBought.setPurchaseTimestamp(Timestamp.valueOf(LocalDateTime.now()));
					productBought.setMaxPrice(last);
					productBought.setMinPrice(last);
					productBought.setCurrentPrice(last);
					productBought.setCurrentPriceTimestamp(Timestamp.valueOf(LocalDateTime.now()));
					productBought.setCurrentPercentage(entry.getValue());

					productRepository.save(productBought);

					if (i == maxNumberCrypto) {
						break;
					}

					i++;
				}

			} else {
				throw new RestClientException("Failed while getting products list");
			}
		} catch (Exception e) {
			logger.error("Error finding crypto to buy : {}", e.getMessage());
		}
		return statsList;
	}

	public static <K, V> void printMap(Map<K, V> map) {
		for (Map.Entry<K, V> entry : map.entrySet()) {
			logger.info("Key : {}, Value : {}", entry.getKey(), entry.getValue());
		}
	}

	public List<PurchasedProductDAO> retrieveAllPurchasedProducts() {
		return productRepository.findAll();
	}
}