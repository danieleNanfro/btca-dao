package com.coinbase.api.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import io.swagger.model.Product;

@FeignClient(name = "${spring.application.name}-products", url = "${com.coinbase.api.products.list.url}", decode404 = true)
public interface ProductCoinBase {

	@GetMapping(value = "/", consumes = "application/json")
	public ResponseEntity<List<Product>> getProducts();
}
