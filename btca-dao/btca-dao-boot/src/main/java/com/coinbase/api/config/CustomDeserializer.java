package com.coinbase.api.config;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import org.springframework.boot.jackson.JsonComponent;
import org.springframework.http.HttpStatus;

import com.coinbase.api.exception.BadFormatRuntimeException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@JsonComponent
public class CustomDeserializer extends JsonDeserializer<OffsetDateTime> {

	private DateTimeFormatter formatter;

	public CustomDeserializer() {

		formatter = new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd'T'HH:mm:ss")
				.appendFraction(ChronoField.MILLI_OF_SECOND, 0, 3, true).appendOffset("+HHMM", "Z").toFormatter();

	}

	@Override
	public OffsetDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {

		String s = parser.getText();
		OffsetDateTime date;

		if ((s.indexOf('Z') == 23 && s.length() == 24) || s.length() == 28) {

			date = OffsetDateTime.parse(s, this.formatter);
			return date;

		}

		if (s.length() == 10) {

			LocalDate dateConv = LocalDate.parse(s, DateTimeFormatter.ISO_LOCAL_DATE);
			s = dateConv.format(DateTimeFormatter.ISO_LOCAL_DATE) + "T00:00:00.000+0000";
			date = OffsetDateTime.parse(s, this.formatter);
			return date;
		}

		throw new BadFormatRuntimeException(
				"Bad format Exception: date must be in the format yyyy-MM-dd or yyyy-MM-ddTHH:mm:ss.SSS+HHMM or yyyy-MM-ddTHH:mm:ss.SSSZ",
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
