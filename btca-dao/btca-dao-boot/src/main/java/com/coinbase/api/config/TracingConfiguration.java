package com.coinbase.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.opentracing.mock.MockTracer;
import io.opentracing.util.GlobalTracer;

@Configuration
public class TracingConfiguration {

	@Value("${spring.application.name}")
	private String applicationName;

	@Bean
	public io.opentracing.Tracer mockTracer() {
		MockTracer mockTracer = new MockTracer();
		GlobalTracer.registerIfAbsent(mockTracer);
		return mockTracer;
	}

}
