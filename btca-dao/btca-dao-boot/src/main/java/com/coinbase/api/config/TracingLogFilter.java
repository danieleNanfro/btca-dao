package com.coinbase.api.config;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Random;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import io.opentracing.Span;
import io.opentracing.SpanContext;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TracingLogFilter implements Filter {

	private Random random = new Random();

	@Value("${spring.application.name}")
	String applicationName;

	@Autowired
	private io.opentracing.Tracer tracer;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		Span span = tracer.buildSpan(applicationName).start();
		SpanContext spanContext = span.context();
		String date = String.valueOf(OffsetDateTime.now().toEpochSecond());
		if (spanContext != null) {
			MDC.put("spanId", concatenate(spanContext.toSpanId(), date));
			MDC.put("traceId", concatenate(spanContext.toTraceId(), date));
		}
		chain.doFilter(request, response);
		if (spanContext != null) {
			MDC.remove("spanId");
			MDC.remove("traceId");
		}
		span.finish();
	}

	private String concatenate(String id, String date) {
		String rand = String.format("%05d", Integer.valueOf(this.random.nextInt(100000)));
		date = StringUtils.leftPad(date, 10, rand);
		id = StringUtils.rightPad(id, 6, rand);

		String conc = id + date;
		return conc.substring(conc.length() - 16, conc.length());
	}

}