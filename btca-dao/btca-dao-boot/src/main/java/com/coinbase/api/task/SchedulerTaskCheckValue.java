package com.coinbase.api.task;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.coinbase.api.dao.PurchasedProductDAO;
import com.coinbase.api.proxy.ProxyCoinBase;
import com.coinbase.api.repository.ProductRepository;

import io.swagger.model.Stats;

@Component
public class SchedulerTaskCheckValue {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SchedulerTaskCheckValue.class);

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProxyCoinBase proxyCoinBase;

	@Value("${com.coinbase.api.min-perc-earn}")
	private int minPercEarn;
	
	@Value("${com.coinbase.api.min-perc-loss}")
	private int minPercLoss;

	@Scheduled(cron = "${com.coinbase.api.cron-time-task-check-value}")
	public void initExtraction() {
		try {

			List<PurchasedProductDAO> productBoughts = productRepository.findAll();

			for (PurchasedProductDAO productBought : productBoughts) {

				logger.info("Task Init Check Value: {}", LocalDateTime.now());

				if (productBought.getSellProduct() == 0) {

					ResponseEntity<Stats> statsResponse = proxyCoinBase.getStatsById(productBought.getIdCrypto());

					if (statsResponse != null) {

						Stats stats = statsResponse.getBody();

						if (stats != null) {

							BigDecimal last = new BigDecimal(stats.getLast());
							BigDecimal buyPrice = productBought.getPurchasePrice();
							BigDecimal perc = last.subtract(buyPrice).divide(buyPrice, 4, RoundingMode.HALF_DOWN);
							BigDecimal maxPrice = productBought.getMaxPrice();
							BigDecimal minPrice = productBought.getMinPrice();
							logger.info("Task #############: id: {} before: {}, now: {}, perc: {}",
									productBought.getIdCrypto(), productBought.getPurchasePrice(), last, perc);

							Timestamp currentTimestamp = Timestamp.valueOf(LocalDateTime.now());

							if (perc.multiply(BigDecimal.valueOf(100)).compareTo(BigDecimal.valueOf(minPercEarn)) > 0) {

								productBought.setSellPrice(last);
								productBought.setSellPercentage(perc);
								productBought.setSellPriceTimestamp(currentTimestamp);
								productBought.setSellProduct(1);
								productBought.setMaxPrice(last);
								productBought.setMaxPercentage(perc);
								productBought.setMaxPriceTimestamp(currentTimestamp);
								productBought.setCurrentPrice(last);
								productBought.setCurrentPercentage(perc);
								productBought.setCurrentPriceTimestamp(currentTimestamp);
								productRepository.save(productBought);
							} else if (perc.multiply(BigDecimal.valueOf(100)).compareTo(BigDecimal.valueOf(minPercLoss)) < 0) {
								productBought.setSellPrice(last);
								productBought.setSellPercentage(perc);
								productBought.setSellPriceTimestamp(currentTimestamp);
								productBought.setSellProduct(1);
								productBought.setMinPrice(last);
								productBought.setMinPercentage(perc);
								productBought.setMinPriceTimestamp(currentTimestamp);
								productBought.setCurrentPrice(last);
								productBought.setCurrentPercentage(perc);
								productBought.setCurrentPriceTimestamp(currentTimestamp);
								productRepository.save(productBought);
							} else if (last.compareTo(maxPrice) > 0) {
								productBought.setMaxPrice(last);
								productBought.setMaxPercentage(perc);
								productBought.setMaxPriceTimestamp(currentTimestamp);
								productBought.setCurrentPrice(last);
								productBought.setCurrentPercentage(perc);
								productBought.setCurrentPriceTimestamp(currentTimestamp);
								productRepository.save(productBought);
							} else if (last.compareTo(minPrice) < 0) {
								productBought.setMinPrice(last);
								productBought.setMinPercentage(perc);
								productBought.setMinPriceTimestamp(currentTimestamp);
								productBought.setCurrentPrice(last);
								productBought.setCurrentPercentage(perc);
								productBought.setCurrentPriceTimestamp(currentTimestamp);
								productRepository.save(productBought);
							} else {
								productBought.setCurrentPrice(last);
								productBought.setCurrentPercentage(perc);
								productBought.setCurrentPriceTimestamp(currentTimestamp);
								productRepository.save(productBought);
							}
						}
					}
				}
			}

			logger.info("Cron Task Finished Check Value : {}", LocalDateTime.now());

		} catch (Exception e) {
			logger.error("Error : {}", e.getMessage());
		}
	}

}