package com.coinbase.api.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class PurchasedProductDAO {

	@Id
	@GeneratedValue
	private Long id;
	private String idCrypto;
	@Column(precision = 16, scale = 8)
	private BigDecimal purchasePrice;
	@Column(precision = 16, scale = 8)
	private BigDecimal maxPrice;
	@Column(precision = 16, scale = 8)
	private BigDecimal minPrice;
	@Column(precision = 16, scale = 8)
	private BigDecimal sellPrice;
	@Column(precision = 16, scale = 8)
	private BigDecimal currentPrice;
	@Column(precision = 16, scale = 4)
	private BigDecimal startPercentage;
	@Column(precision = 16, scale = 4)
	private BigDecimal maxPercentage;
	@Column(precision = 16, scale = 4)
	private BigDecimal minPercentage;
	@Column(precision = 16, scale = 4)
	private BigDecimal sellPercentage;
	@Column(precision = 16, scale = 4)
	private BigDecimal currentPercentage;
	private Timestamp purchaseTimestamp;
	private Timestamp maxPriceTimestamp;
	private Timestamp minPriceTimestamp;
	private Timestamp sellPriceTimestamp;
	private Timestamp currentPriceTimestamp;
	private int sellProduct;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdCrypto() {
		return idCrypto;
	}

	public void setIdCrypto(String idCrypto) {
		this.idCrypto = idCrypto;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}

	public BigDecimal getStartPercentage() {
		return startPercentage;
	}

	public void setStartPercentage(BigDecimal startPercentage) {
		this.startPercentage = startPercentage;
	}

	public BigDecimal getMaxPercentage() {
		return maxPercentage;
	}

	public void setMaxPercentage(BigDecimal maxPercentage) {
		this.maxPercentage = maxPercentage;
	}

	public BigDecimal getMinPercentage() {
		return minPercentage;
	}

	public void setMinPercentage(BigDecimal minPercentage) {
		this.minPercentage = minPercentage;
	}

	public BigDecimal getSellPercentage() {
		return sellPercentage;
	}

	public void setSellPercentage(BigDecimal sellPercentage) {
		this.sellPercentage = sellPercentage;
	}

	public BigDecimal getCurrentPercentage() {
		return currentPercentage;
	}

	public void setCurrentPercentage(BigDecimal currentPercentage) {
		this.currentPercentage = currentPercentage;
	}

	public Timestamp getPurchaseTimestamp() {
		return purchaseTimestamp;
	}

	public void setPurchaseTimestamp(Timestamp purchaseTimestamp) {
		this.purchaseTimestamp = purchaseTimestamp;
	}

	public Timestamp getMaxPriceTimestamp() {
		return maxPriceTimestamp;
	}

	public void setMaxPriceTimestamp(Timestamp maxPriceTimestamp) {
		this.maxPriceTimestamp = maxPriceTimestamp;
	}

	public Timestamp getMinPriceTimestamp() {
		return minPriceTimestamp;
	}

	public void setMinPriceTimestamp(Timestamp minPriceTimestamp) {
		this.minPriceTimestamp = minPriceTimestamp;
	}

	public Timestamp getSellPriceTimestamp() {
		return sellPriceTimestamp;
	}

	public void setSellPriceTimestamp(Timestamp sellPriceTimestamp) {
		this.sellPriceTimestamp = sellPriceTimestamp;
	}

	public Timestamp getCurrentPriceTimestamp() {
		return currentPriceTimestamp;
	}

	public void setCurrentPriceTimestamp(Timestamp currentPriceTimestamp) {
		this.currentPriceTimestamp = currentPriceTimestamp;
	}

	public int getSellProduct() {
		return sellProduct;
	}

	public void setSellProduct(int sellProduct) {
		this.sellProduct = sellProduct;
	}
}