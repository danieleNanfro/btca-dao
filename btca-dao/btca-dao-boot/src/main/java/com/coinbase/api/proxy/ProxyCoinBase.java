package com.coinbase.api.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import io.swagger.model.Stats;

@FeignClient(name = "${spring.application.name}", url = "${com.coinbase.api.products.list.url}", decode404 = true)
public interface ProxyCoinBase {

	@GetMapping(value = "/{id}/stats", consumes = "application/json")
	public ResponseEntity<Stats> getStatsById(@PathVariable("id") String id);
}
