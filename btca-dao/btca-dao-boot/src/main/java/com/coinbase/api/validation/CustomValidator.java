package com.coinbase.api.validation;

import org.springframework.validation.Validator;

public interface CustomValidator extends Validator {

	@Override
	public default boolean supports(Class<?> clazz) {
		return getType().isAssignableFrom(clazz);
	}

	public Class<?> getType();

}
