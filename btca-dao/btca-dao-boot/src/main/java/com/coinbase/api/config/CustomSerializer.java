package com.coinbase.api.config;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@JsonComponent
public class CustomSerializer extends JsonSerializer<OffsetDateTime> {

	private DateTimeFormatter formatter;

	public CustomSerializer() {

		formatter = new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd'T'HH:mm:ss")
				.appendFraction(ChronoField.MILLI_OF_SECOND, 0, 3, true).appendOffset("+HHMM", "Z").toFormatter();

	}

	@Override
	public void serialize(OffsetDateTime date, JsonGenerator generator, SerializerProvider provider)
			throws IOException {

		String s = date.format(formatter);
		if (s.length() == 20)
			s = s.substring(0, 19) + ".000Z";
		generator.writeString(s);
	}

}