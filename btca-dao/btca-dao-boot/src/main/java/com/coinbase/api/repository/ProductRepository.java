package com.coinbase.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.coinbase.api.dao.PurchasedProductDAO;

@Repository
public interface ProductRepository extends JpaRepository<PurchasedProductDAO, Long> {

}