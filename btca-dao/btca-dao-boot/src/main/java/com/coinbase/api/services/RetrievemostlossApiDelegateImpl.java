package com.coinbase.api.services;

import static com.coinbase.api.utils.MapperConstants.instanceProduct;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.WebDataBinder;

import com.coinbase.api.dao.PurchasedProductDAO;
import com.coinbase.api.validation.ValidatorDelegate;

import io.swagger.api.ProductsApiDelegate;
import io.swagger.model.PurchasedListResponse;
import io.swagger.model.PurchasedProduct;
import io.swagger.model.Stats;
import io.swagger.model.StatsListResponse;

@Service
public class RetrievemostlossApiDelegateImpl implements ProductsApiDelegate {

	@Autowired
	private CoinBaseService coinBaseService;

	@Autowired
	private ValidatorDelegate validatorDelegate;

	@Value("${com.coinbase.api.products.number}")
	private int maxNumberCrypto;

	@Override
	public ResponseEntity<StatsListResponse> productsLossesGet() {

		List<Stats> statsList = coinBaseService.calculateMostLoss(maxNumberCrypto);

		StatsListResponse statsListResponse = new StatsListResponse();
		statsListResponse.setEsito(!statsList.isEmpty());
		statsListResponse.setStats(statsList);

		return ResponseEntity.ok(statsListResponse);
	}

	@Override
	public ResponseEntity<PurchasedListResponse> productsPurchasedGet() {

		List<PurchasedProductDAO> productsPurchasedDAOList = coinBaseService.retrieveAllPurchasedProducts();
		List<PurchasedProduct> productsPurchasedList = instanceProduct.internalMap(productsPurchasedDAOList);

		PurchasedListResponse PurchasedListResponse = new PurchasedListResponse();
		PurchasedListResponse.setEsito(true);
		PurchasedListResponse.setProducts(productsPurchasedList);
		return ResponseEntity.ok(PurchasedListResponse);

	}

	@Override
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validatorDelegate);
	}
}
