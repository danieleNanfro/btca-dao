package com.coinbase.api.utils;

import org.mapstruct.factory.Mappers;

import com.coinbase.api.converter.ProductMapper;

public class MapperConstants {

	private MapperConstants() {
		throw new IllegalStateException("Utility class");
	}

	public static final ProductMapper instanceProduct = Mappers.getMapper(ProductMapper.class);

}
