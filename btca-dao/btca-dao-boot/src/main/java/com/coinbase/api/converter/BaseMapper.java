package com.coinbase.api.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import org.mapstruct.Mapper;

@Mapper
public interface BaseMapper {

	default java.time.OffsetDateTime map(java.util.Date value) {
		if (value == null)
			return null;
		return LocalDateTime.ofInstant(value.toInstant(), ZoneId.systemDefault()).atOffset(ZoneOffset.UTC);
	}

	default java.util.Date map(java.time.OffsetDateTime value) {
		if (value == null)
			return null;
		return Date.from(value.toInstant());
	}
}
