package com.coinbase.api.converter;

import java.util.List;

import org.mapstruct.Mapper;

import com.coinbase.api.dao.PurchasedProductDAO;

import io.swagger.model.PurchasedProduct;

@Mapper(uses = BaseMapper.class)
public interface ProductMapper {

	public List<PurchasedProduct> internalMap(List<PurchasedProductDAO> productsList);

	PurchasedProduct internalConverter(PurchasedProductDAO product);

}