package com.coinbase.api.exception;

import org.springframework.http.HttpStatus;

public class ApiRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final HttpStatus httpStatus;

	public ApiRequestException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public ApiRequestException(String message, Throwable cause) {
		super(message, cause);
		this.httpStatus = getStatus();
	}

	public HttpStatus getStatus() {
		return this.httpStatus != null ? this.httpStatus : HttpStatus.NOT_FOUND;
	}
}