package com.coinbase.api.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = { ApiRequestException.class })
	public ResponseEntity<Object> handleBadRequestException(ApiRequestException e) {

		ErrorResult errorResult = new ErrorResult(e.getMessage(), e.getStatus(), ZonedDateTime.now(ZoneId.of("Z")));

		return new ResponseEntity<>(errorResult, errorResult.getStatus());
	}

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> handleBadRequestException(Exception e) {

		ErrorResult errorResult = new ErrorResult(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR,
				ZonedDateTime.now(ZoneId.of("Z")));

		return new ResponseEntity<>(errorResult, errorResult.getStatus());
	}
}