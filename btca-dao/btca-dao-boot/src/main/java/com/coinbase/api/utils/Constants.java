
package com.coinbase.api.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constants {

	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";

	public static final List<Integer> listaBanche = Collections.unmodifiableList((Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8,
			9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 33, 34, 35, 37, 39, 42,
			43, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71,
			72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 91, 92, 94, 96, 99, 100, 101, 102, 103, 104, 105,
			0106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126,
			127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 145, 146, 147, 148,
			149, 151, 153, 154, 155, 156, 158, 159, 160, 161, 163, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174,
			175, 176, 177, 178, 179, 180, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196,
			197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217,
			218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238,
			239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259,
			260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280,
			281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301,
			302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322,
			323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343,
			344, 345, 346, 347, 348, 349, 350, 352, 353, 354, 355, 356)));

	public static final List<String> listaCanali = Collections.unmodifiableList((Arrays.asList("ATM", "BAN", "BAT",
			"CAL", "CHI", "CSR", "ECO", "GSM", "HBN", "INP", "INT", "IVR", "MOB", "MOF", "PFI", "POS", "PUL", "RBN",
			"RNI", "SEL", "SEV", "SOL", "SPO", "SPR", "SUP", "TES", "TPP", "UMT", "WAP", "WET")));

	public static final String MDC_TRACE_ID = "traceId";
	public static final String FROM_CACHE = "Response from cache";
	public static final String TRY_FROM_CACHE = "Null response from DAO service. Trying to get response from cache";

	public static final String DATAGRID_EXCEPTION = "Datagrid Exception";

	private Constants() { // Compliant
		throw new IllegalStateException("Utility class");
	}
}
